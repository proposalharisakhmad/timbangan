#include "HX711.h"  //You must have this library in your arduino library folder
#include <PubSubClient.h> 
#include <ESP8266WiFi.h>
#define DOUT  D1
#define CLK  D2
 
HX711 scale(DOUT, CLK);
//
char* ssid = "IOT_NET";
char* password = "12345678";


const char* mqtt_server = "mqtt.iot.asmat.app";
const char* HostName = "mqtt.iot.asmat.app";
const char* mqttUser = "tiketux-timbangan";
const char* mqttPassword = "";
const char* topik = "timbangan";
WiFiClient espClient;
PubSubClient client(espClient);

 
float calibration_factor = -91600; 
float timbangan; 

void reconnect() {
  while (!client.connected()) {
//    Serial.print("Attempting MQTT connection...");
    if (client.connect(HostName, mqttUser, mqttPassword)) {
//      Serial.println("connected");
      client.subscribe(topik, 2);
      
    }
    else{
//      Serial.print("failed, rc=");
//      Serial.print(client.state());
//      Serial.println(" try again in 5 seconds");
      delay(5000);
      return;
    }
}
}


void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
//  Serial.println();
//  Serial.print("Connecting to ");
//  Serial.println(ssid);

  WiFi.begin(ssid, password);
//  Serial.print("sudah konek ");

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
//    Serial.print(".");
  }

//  Serial.println("");
//  Serial.println("WiFi connected");
//  Serial.println("IP address: ");
//  Serial.println(WiFi.localIP());
}



void setup() {
  Serial.begin(9600);
  setup_wifi();
  scale.set_scale();
  scale.tare(); //Reset the scale to 0
 
  long zero_factor = scale.read_average(); //Get a baseline reading
//  Serial.print("Zero factor: "); //This can be used to remove the need to tare the scale. Useful in permanent scale projects.
//  Serial.println(zero_factor);
  
  client.setServer(mqtt_server, 1883);
//  client.setCallback(callback);
}
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
 
  scale.set_scale(calibration_factor); //Adjust to this calibration factor
 
  timbangan=scale.get_units(),3;
//  Serial.print("hasil: ");
  
  Serial.println(timbangan);
//  Serial.print(" kg"); 

  char timbanganString[8];
  dtostrf(timbangan, 1, 2, timbanganString);

  
  client.publish("timbangan", timbanganString);
  delay(1000);
//  Serial.println();
 
}
